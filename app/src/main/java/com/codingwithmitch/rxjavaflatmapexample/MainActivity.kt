package com.codingwithmitch.rxjavaflatmapexample

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codingwithmitch.rxjavaflatmapexample.models.Post
import com.codingwithmitch.rxjavaflatmapexample.requests.ServiceGenerator
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    //ui
    private var recyclerView: RecyclerView? = null

    // vars
    private val disposables = CompositeDisposable()
    private var adapter: RecyclerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recycler_view)
        initRecyclerView()

        //RETRIEVES THE OBJECTS IN NO PARTICULAR ORDER.
//        createObservableWithFlatMapOperator()

        //RETRIEVES THE OBJECTS IN ORDER.
        createObservableWithConcatMapOperator()
    }

    private fun createObservableWithConcatMapOperator() {
        getPostsObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .concatMap { post -> getCommentsObservable(post) }
                .subscribe(object : Observer<Post> {
                    override fun onComplete() {}

                    override fun onSubscribe(d: Disposable) {
                        disposables.add(d)
                    }

                    override fun onNext(post: Post) {
                        updatePost(post)
                    }

                    override fun onError(e: Throwable) {
                        Log.e("ErrorSubscriber", "onError: ", e)
                    }
                })
    }

    private fun createObservableWithFlatMapOperator() {
        getPostsObservable()
                .subscribeOn(Schedulers.io())
                .flatMap { post -> getCommentsObservable(post) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<Post> {
                    override fun onComplete() {}

                    override fun onSubscribe(d: Disposable) {
                        disposables.add(d)
                    }

                    override fun onNext(post: Post) {
                        Log.d("TAG", "post retrieved")
                        updatePost(post)
                    }

                    override fun onError(e: Throwable) {
                        Log.e("ErrorSubscriber", "onError", e)
                    }

                })
    }

    private fun updatePost(post: Post) {
        adapter!!.updatePost(post)
    }

    private fun getCommentsObservable(post: Post): Observable<Post> {
        return ServiceGenerator.requestApi.getComments(post.id)
                .map { commentsList ->
                    val interval = (Random.nextInt(5) + 1) * 1000
                    Thread.sleep(interval.toLong())
                    Log.d("TAG", "apply sleeping thread: ${Thread.currentThread().name} for $interval ms")
                    post.apply { comments = commentsList }
                }
                .subscribeOn(Schedulers.io())
    }

    private fun getPostsObservable(): Observable<Post> {
        return ServiceGenerator
                .requestApi.getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap { postList ->
                    adapter!!.setPosts(postList.toMutableList())
                    Observable.fromIterable(postList).subscribeOn(Schedulers.io())
                }
    }

    private fun initRecyclerView() {
        adapter = RecyclerAdapter()
        recyclerView!!.layoutManager = LinearLayoutManager(this)
        recyclerView!!.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}